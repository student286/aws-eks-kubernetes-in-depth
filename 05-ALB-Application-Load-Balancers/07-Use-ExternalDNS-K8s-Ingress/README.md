---
title: AWS Load Balancer Controller - External DNS & Ingress
description: Learn AWS Load Balancer Controller - External DNS & Ingress
---

## Step-01: Update Ingress manifest by adding External DNS Annotation
- Added annotation with two DNS Names
  - one.architectprashant.com
  - two.architectprashant.com
- Once we deploy the application, we should be able to access our Applications with both DNS Names.   
```yaml
    # External DNS - For creating a Record Set in Route53
    external-dns.alpha.kubernetes.io/hostname: one.architectprashant.com, two.architectprashant.com
```

## Step-02: Deploy all Application Kubernetes Manifests
### Deploy
```t
# Deploy kube-manifests
kubectl apply -f kube-manifests/

# Verify Ingress Resource
kubectl get ingress

# Verify Apps
kubectl get deploy
kubectl get pods

# Verify NodePort Services
kubectl get svc
```
### Verify Load Balancer & Target Groups
- Load Balancer -  Listeneres (Verify both 80 & 443) 
- Load Balancer - Rules (Verify both 80 & 443 listeners) 
- Target Groups - Group Details (Verify Health check path)
- Target Groups - Targets (Verify all 3 targets are healthy)

### Verify External DNS Log
```t
# Verify External DNS logs
kubectl logs -f $(kubectl get po | egrep -o 'external-dns[A-Za-z0-9-]+')
```
### Verify Route53
- Go to Services -> Route53
- You should see **Record Sets** added for `one.architectprashant.com`, `two.architectprashant.com`

## Step-04: Access Application using newly registered DNS Name
### Perform nslookup tests before accessing Application
- Test if our new DNS entries registered and resolving to an IP Address
```t
# nslookup commands
nslookup one.architectprashant.com
nslookup two.architectprashant.com
```
### Access Application using one domain
```t
# HTTP URLs (Should Redirect to HTTPS)
http://one.architectprashant.com/app1/index.html
http://one.architectprashant.com/app2/index.html
http://one.architectprashant.com/
```

### Access Application using two domain
```t
# HTTP URLs (Should Redirect to HTTPS)
http://two.architectprashant.com/app1/index.html
http://two.architectprashant.com/app2/index.html
http://two.architectprashant.com
```


## Step-05: Clean Up
```t
# Delete Manifests
kubectl delete -f kube-manifests/

```


## References
- https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/alb-ingress.md
- https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/aws.md


